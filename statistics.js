/*
Request URL: https://chrome.google.com/webstore/developer/data?hl=ru&tq=base_stats%3A< YOUR ID HERE >&tqx=out%3Ajson
Request URL: https://chrome.google.com/webstore/developer/data?hl=ru&tq=weekly_users_stats%3A< YOUR ID HERE >&tqx=out%3Ajson
Request URL: https://chrome.google.com/webstore/developer/data?hl=ru&tq=uninstall_stats%3A< YOUR ID HERE >&tqx=out%3Ajson
*/

let page = null;

/*
{
  "version": "0.6",
  "status": "ok",
  "sig": "700855201",
  "table": {
    "cols": [
      {
        "id": "date",
        "label": "Число",
        "type": "date",
        "pattern": ""
      },
      {
        "id": "ekcgdapokfgdppdfmhbmhenidkjalgjfImpressionsAll",
        "label": "Показы",
        "type": "number",
        "pattern": "",
        "p": {
          "item": "Обход блокировки Telegram",
          "metric": "Показы",
          "source": "Все источники"
        }
      },
      {
        "id": "ekcgdapokfgdppdfmhbmhenidkjalgjfInstallationsAll",
        "label": "Установки",
        "type": "number",
        "pattern": "",
        "p": {
          "item": "Обход блокировки Telegram",
          "metric": "Установки",
          "source": "Все источники"
        }
      }
    ],
    "rows": [
      {
        "c": [
          {
            "v": "Date(2018,3,16)"
          },
          {
            "v": null
          },
          {
            "v": null
          }
        ]
      }
    ]
  }
}
*/

set_page = async (value) => {
  page = value;
};

get_base = async (id) => {
  let url = 'https://chrome.google.com/webstore/developer/data?hl=ru&tq=base_stats:'+ id +'&tqx=out:json';
  const status = await page.open(url);

  if(status == "success") {
    let data = await page.property('plainText');
    data = data.replace(/\,\,/gi, ',{ "v": null },');
    try {
      return await JSON.parse(data);
    } catch(e) {
      return null;
    }
  }
};

/*

{
  "version": "0.6",
  "status": "ok",
  "sig": "1745544555",
  "table": {
    "cols": [
      {
        "id": "date",
        "label": "Число",
        "type": "date",
        "pattern": ""
      },
      {
        "id": "ekcgdapokfgdppdfmhbmhenidkjalgjfWeeklyUsersAll",
        "label": "Всего текущих пользователей",
        "type": "number",
        "pattern": "",
        "p": {
          "item": "Обход блокировки Telegram",
          "metric": "Всего текущих пользователей",
          "source": "Все источники"
        }
      }
    ],
    "rows": [
      {
        "c": [
          {
            "v": "Date(2018,3,16)"
          },
          {
            "v": null
          }
        ]
      }
    ]
  }
}

*/

get_weekly_users = async (id) => {
  let url = 'https://chrome.google.com/webstore/developer/data?hl=ru&tq=weekly_users_stats:'+ id +'&tqx=out:json';
  const status = await page.open(url);

  if(status == "success") {
    let data = await page.property('plainText');
    try {
      return await JSON.parse(data);
    } catch(e) {
      return null;
    }
  }
  //return await JSON.parse('{"version":"0.6","status":"ok","sig":"1745544555","table":{"cols":[{"id":"date","label":"Число","type":"date","pattern":""},{"id":"ekcgdapokfgdppdfmhbmhenidkjalgjfWeeklyUsersAll","label":"Всего текущих пользователей","type":"number","pattern":"","p":{"item":"Обход блокировки Telegram","metric":"Всего текущих пользователей","source":"Все источники"}}],"rows":[{"c":[{"v":"Date(2018,3,16)"},{"v":null}]},{"c":[{"v":"Date(2018,3,17)"},{"v":null}]},{"c":[{"v":"Date(2018,3,18)"},{"v":null}]},{"c":[{"v":"Date(2018,3,19)"},{"v":null}]},{"c":[{"v":"Date(2018,3,20)"},{"v":null}]},{"c":[{"v":"Date(2018,3,21)"},{"v":null}]},{"c":[{"v":"Date(2018,3,22)"},{"v":null}]},{"c":[{"v":"Date(2018,3,23)"},{"v":null}]},{"c":[{"v":"Date(2018,3,24)"},{"v":null}]},{"c":[{"v":"Date(2018,3,25)"},{"v":null}]},{"c":[{"v":"Date(2018,3,26)"},{"v":null}]},{"c":[{"v":"Date(2018,3,27)"},{"v":null}]},{"c":[{"v":"Date(2018,3,28)"},{"v":null}]},{"c":[{"v":"Date(2018,3,29)"},{"v":null}]},{"c":[{"v":"Date(2018,3,30)"},{"v":null}]},{"c":[{"v":"Date(2018,4,1)"},{"v":null}]},{"c":[{"v":"Date(2018,4,2)"},{"v":null}]},{"c":[{"v":"Date(2018,4,3)"},{"v":null}]},{"c":[{"v":"Date(2018,4,4)"},{"v":null}]},{"c":[{"v":"Date(2018,4,5)"},{"v":null}]},{"c":[{"v":"Date(2018,4,6)"},{"v":null}]},{"c":[{"v":"Date(2018,4,7)"},{"v":null}]},{"c":[{"v":"Date(2018,4,8)"},{"v":null}]},{"c":[{"v":"Date(2018,4,9)"},{"v":null}]},{"c":[{"v":"Date(2018,4,10)"},{"v":null}]},{"c":[{"v":"Date(2018,4,11)"},{"v":null}]},{"c":[{"v":"Date(2018,4,12)"},{"v":null}]},{"c":[{"v":"Date(2018,4,13)"},{"v":null}]},{"c":[{"v":"Date(2018,4,14)"},{"v":null}]},{"c":[{"v":"Date(2018,4,15)"},{"v":null}]},{"c":[{"v":"Date(2018,4,16)"},{"v":null}]},{"c":[{"v":"Date(2018,4,17)"},{"v":null}]},{"c":[{"v":"Date(2018,4,18)"},{"v":null}]},{"c":[{"v":"Date(2018,4,19)"},{"v":null}]},{"c":[{"v":"Date(2018,4,20)"},{"v":null}]},{"c":[{"v":"Date(2018,4,21)"},{"v":null}]},{"c":[{"v":"Date(2018,4,22)"},{"v":null}]},{"c":[{"v":"Date(2018,4,23)"},{"v":null}]},{"c":[{"v":"Date(2018,4,24)"},{"v":null}]},{"c":[{"v":"Date(2018,4,25)"},{"v":null}]},{"c":[{"v":"Date(2018,4,26)"},{"v":null}]},{"c":[{"v":"Date(2018,4,27)"},{"v":null}]},{"c":[{"v":"Date(2018,4,28)"},{"v":null}]},{"c":[{"v":"Date(2018,4,29)"},{"v":null}]},{"c":[{"v":"Date(2018,4,30)"},{"v":null}]},{"c":[{"v":"Date(2018,4,31)"},{"v":null}]},{"c":[{"v":"Date(2018,5,1)"},{"v":null}]},{"c":[{"v":"Date(2018,5,2)"},{"v":null}]},{"c":[{"v":"Date(2018,5,3)"},{"v":null}]},{"c":[{"v":"Date(2018,5,4)"},{"v":null}]},{"c":[{"v":"Date(2018,5,5)"},{"v":null}]},{"c":[{"v":"Date(2018,5,6)"},{"v":null}]},{"c":[{"v":"Date(2018,5,7)"},{"v":null}]},{"c":[{"v":"Date(2018,5,8)"},{"v":null}]},{"c":[{"v":"Date(2018,5,9)"},{"v":null}]},{"c":[{"v":"Date(2018,5,10)"},{"v":null}]},{"c":[{"v":"Date(2018,5,11)"},{"v":null}]},{"c":[{"v":"Date(2018,5,12)"},{"v":null}]},{"c":[{"v":"Date(2018,5,13)"},{"v":null}]},{"c":[{"v":"Date(2018,5,14)"},{"v":null}]},{"c":[{"v":"Date(2018,5,15)"},{"v":null}]},{"c":[{"v":"Date(2018,5,16)"},{"v":null}]},{"c":[{"v":"Date(2018,5,17)"},{"v":null}]},{"c":[{"v":"Date(2018,5,18)"},{"v":null}]},{"c":[{"v":"Date(2018,5,19)"},{"v":null}]},{"c":[{"v":"Date(2018,5,20)"},{"v":null}]},{"c":[{"v":"Date(2018,5,21)"},{"v":null}]},{"c":[{"v":"Date(2018,5,22)"},{"v":null}]},{"c":[{"v":"Date(2018,5,23)"},{"v":null}]},{"c":[{"v":"Date(2018,5,24)"},{"v":null}]},{"c":[{"v":"Date(2018,5,25)"},{"v":null}]},{"c":[{"v":"Date(2018,5,26)"},{"v":null}]},{"c":[{"v":"Date(2018,5,27)"},{"v":null}]},{"c":[{"v":"Date(2018,5,28)"},{"v":null}]},{"c":[{"v":"Date(2018,5,29)"},{"v":null}]},{"c":[{"v":"Date(2018,5,30)"},{"v":null}]},{"c":[{"v":"Date(2018,6,1)"},{"v":null}]},{"c":[{"v":"Date(2018,6,2)"},{"v":null}]},{"c":[{"v":"Date(2018,6,3)"},{"v":null}]},{"c":[{"v":"Date(2018,6,4)"},{"v":null}]},{"c":[{"v":"Date(2018,6,5)"},{"v":null}]},{"c":[{"v":"Date(2018,6,6)"},{"v":null}]},{"c":[{"v":"Date(2018,6,7)"},{"v":null}]},{"c":[{"v":"Date(2018,6,8)"},{"v":null}]},{"c":[{"v":"Date(2018,6,9)"},{"v":null}]},{"c":[{"v":"Date(2018,6,10)"},{"v":null}]},{"c":[{"v":"Date(2018,6,11)"},{"v":2.0}]},{"c":[{"v":"Date(2018,6,12)"},{"v":4.0}]},{"c":[{"v":"Date(2018,6,13)"},{"v":4.0}]},{"c":[{"v":"Date(2018,6,14)"},{"v":4.0}]},{"c":[{"v":"Date(2018,6,15)"},{"v":4.0}]}]}}');
};

/*

{
  "version": "0.6",
  "status": "ok",
  "sig": "704384134",
  "table": {
    "cols": [
      {
        "id": "date",
        "label": "Число",
        "type": "date",
        "pattern": ""
      },
      {
        "id": "ekcgdapokfgdppdfmhbmhenidkjalgjfUninstallationsAll",
        "label": "Сколько раз удалено",
        "type": "number",
        "pattern": "",
        "p": {
          "item": "Обход блокировки Telegram",
          "metric": "Сколько раз удалено",
          "source": "Все источники"
        }
      }
    ],
    "rows": [
      {
        "c": [
          {
            "v": "Date(2018,3,16)"
          },
          {
            "v": null
          }
        ]
      }
    ]
  }
}

*/

get_uninstall = async (id) => {
  let url = 'https://chrome.google.com/webstore/developer/data?hl=ru&tq=uninstall_stats:'+ id +'&tqx=out:json';
  const status = await page.open(url);

  if(status == "success") {
    let data = await page.property('plainText');
    try {
      return await JSON.parse(data);
    } catch(e) {
      return null;
    }
  }
  //return await JSON.parse('{"version":"0.6","status":"ok","sig":"704384134","table":{"cols":[{"id":"date","label":"Число","type":"date","pattern":""},{"id":"ekcgdapokfgdppdfmhbmhenidkjalgjfUninstallationsAll","label":"Сколько раз удалено","type":"number","pattern":"","p":{"item":"Обход блокировки Telegram","metric":"Сколько раз удалено","source":"Все источники"}}],"rows":[{"c":[{"v":"Date(2018,3,16)"},{"v":null}]},{"c":[{"v":"Date(2018,3,17)"},{"v":null}]},{"c":[{"v":"Date(2018,3,18)"},{"v":null}]},{"c":[{"v":"Date(2018,3,19)"},{"v":null}]},{"c":[{"v":"Date(2018,3,20)"},{"v":null}]},{"c":[{"v":"Date(2018,3,21)"},{"v":null}]},{"c":[{"v":"Date(2018,3,22)"},{"v":null}]},{"c":[{"v":"Date(2018,3,23)"},{"v":null}]},{"c":[{"v":"Date(2018,3,24)"},{"v":null}]},{"c":[{"v":"Date(2018,3,25)"},{"v":null}]},{"c":[{"v":"Date(2018,3,26)"},{"v":null}]},{"c":[{"v":"Date(2018,3,27)"},{"v":null}]},{"c":[{"v":"Date(2018,3,28)"},{"v":null}]},{"c":[{"v":"Date(2018,3,29)"},{"v":null}]},{"c":[{"v":"Date(2018,3,30)"},{"v":null}]},{"c":[{"v":"Date(2018,4,1)"},{"v":null}]},{"c":[{"v":"Date(2018,4,2)"},{"v":null}]},{"c":[{"v":"Date(2018,4,3)"},{"v":null}]},{"c":[{"v":"Date(2018,4,4)"},{"v":null}]},{"c":[{"v":"Date(2018,4,5)"},{"v":null}]},{"c":[{"v":"Date(2018,4,6)"},{"v":null}]},{"c":[{"v":"Date(2018,4,7)"},{"v":null}]},{"c":[{"v":"Date(2018,4,8)"},{"v":null}]},{"c":[{"v":"Date(2018,4,9)"},{"v":null}]},{"c":[{"v":"Date(2018,4,10)"},{"v":null}]},{"c":[{"v":"Date(2018,4,11)"},{"v":null}]},{"c":[{"v":"Date(2018,4,12)"},{"v":null}]},{"c":[{"v":"Date(2018,4,13)"},{"v":null}]},{"c":[{"v":"Date(2018,4,14)"},{"v":null}]},{"c":[{"v":"Date(2018,4,15)"},{"v":null}]},{"c":[{"v":"Date(2018,4,16)"},{"v":null}]},{"c":[{"v":"Date(2018,4,17)"},{"v":null}]},{"c":[{"v":"Date(2018,4,18)"},{"v":null}]},{"c":[{"v":"Date(2018,4,19)"},{"v":null}]},{"c":[{"v":"Date(2018,4,20)"},{"v":null}]},{"c":[{"v":"Date(2018,4,21)"},{"v":null}]},{"c":[{"v":"Date(2018,4,22)"},{"v":null}]},{"c":[{"v":"Date(2018,4,23)"},{"v":null}]},{"c":[{"v":"Date(2018,4,24)"},{"v":null}]},{"c":[{"v":"Date(2018,4,25)"},{"v":null}]},{"c":[{"v":"Date(2018,4,26)"},{"v":null}]},{"c":[{"v":"Date(2018,4,27)"},{"v":null}]},{"c":[{"v":"Date(2018,4,28)"},{"v":null}]},{"c":[{"v":"Date(2018,4,29)"},{"v":null}]},{"c":[{"v":"Date(2018,4,30)"},{"v":null}]},{"c":[{"v":"Date(2018,4,31)"},{"v":null}]},{"c":[{"v":"Date(2018,5,1)"},{"v":null}]},{"c":[{"v":"Date(2018,5,2)"},{"v":null}]},{"c":[{"v":"Date(2018,5,3)"},{"v":null}]},{"c":[{"v":"Date(2018,5,4)"},{"v":null}]},{"c":[{"v":"Date(2018,5,5)"},{"v":null}]},{"c":[{"v":"Date(2018,5,6)"},{"v":null}]},{"c":[{"v":"Date(2018,5,7)"},{"v":null}]},{"c":[{"v":"Date(2018,5,8)"},{"v":null}]},{"c":[{"v":"Date(2018,5,9)"},{"v":null}]},{"c":[{"v":"Date(2018,5,10)"},{"v":null}]},{"c":[{"v":"Date(2018,5,11)"},{"v":null}]},{"c":[{"v":"Date(2018,5,12)"},{"v":null}]},{"c":[{"v":"Date(2018,5,13)"},{"v":null}]},{"c":[{"v":"Date(2018,5,14)"},{"v":null}]},{"c":[{"v":"Date(2018,5,15)"},{"v":null}]},{"c":[{"v":"Date(2018,5,16)"},{"v":null}]},{"c":[{"v":"Date(2018,5,17)"},{"v":null}]},{"c":[{"v":"Date(2018,5,18)"},{"v":null}]},{"c":[{"v":"Date(2018,5,19)"},{"v":null}]},{"c":[{"v":"Date(2018,5,20)"},{"v":null}]},{"c":[{"v":"Date(2018,5,21)"},{"v":null}]},{"c":[{"v":"Date(2018,5,22)"},{"v":null}]},{"c":[{"v":"Date(2018,5,23)"},{"v":null}]},{"c":[{"v":"Date(2018,5,24)"},{"v":null}]},{"c":[{"v":"Date(2018,5,25)"},{"v":null}]},{"c":[{"v":"Date(2018,5,26)"},{"v":null}]},{"c":[{"v":"Date(2018,5,27)"},{"v":null}]},{"c":[{"v":"Date(2018,5,28)"},{"v":null}]},{"c":[{"v":"Date(2018,5,29)"},{"v":null}]},{"c":[{"v":"Date(2018,5,30)"},{"v":null}]},{"c":[{"v":"Date(2018,6,1)"},{"v":null}]},{"c":[{"v":"Date(2018,6,2)"},{"v":null}]},{"c":[{"v":"Date(2018,6,3)"},{"v":null}]},{"c":[{"v":"Date(2018,6,4)"},{"v":null}]},{"c":[{"v":"Date(2018,6,5)"},{"v":null}]},{"c":[{"v":"Date(2018,6,6)"},{"v":null}]},{"c":[{"v":"Date(2018,6,7)"},{"v":null}]},{"c":[{"v":"Date(2018,6,8)"},{"v":null}]},{"c":[{"v":"Date(2018,6,9)"},{"v":null}]},{"c":[{"v":"Date(2018,6,10)"},{"v":null}]},{"c":[{"v":"Date(2018,6,11)"},{"v":null}]},{"c":[{"v":"Date(2018,6,12)"},{"v":2.0}]},{"c":[{"v":"Date(2018,6,13)"},{"v":0.0}]},{"c":[{"v":"Date(2018,6,14)"},{"v":0.0}]},{"c":[{"v":"Date(2018,6,15)"},{"v":1.0}]}]}}');
};

exports.set_page = set_page;
exports.get_base = get_base;
exports.get_weekly_users = get_weekly_users;
exports.get_uninstall = get_uninstall;