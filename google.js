const urls = require('./config/urls.json');
const timeout = require('./utils').timeout;
let page = null;

set_page = async (value) => {
    page = value;
}

const waitFor = (testFx, timeOutMillis) => new Promise(res => {
    var maxtimeOutMillis = timeOutMillis ? timeOutMillis : 3000, //< Default Max Timout is 3s
    start = new Date().getTime(),
    condition = false,

    interval = setInterval(() => {
        if ( (new Date().getTime() - start < maxtimeOutMillis) && !condition ) {
            // If not time-out yet and condition not yet fulfilled
            condition = (typeof(testFx) === "string" ? eval(testFx) : testFx()); //< defensive code
        } else {
            if(!condition) {
                // If condition still not fulfilled (timeout but condition is 'false')
                console.log("'waitFor()' timeout");
            } else {
                // Condition fulfilled (timeout and/or condition is 'true')
                console.log("'waitFor()' finished in " + (new Date().getTime() - start) + "ms.");
            }
            clearInterval(interval);
            res();
        }
    }, 250);
});

set_email = async (email) => {
    console.log('set email');
    await page.evaluate(function(email) {
        var inputEmail = document.getElementById('identifierId');
        inputEmail.click();
        inputEmail.value = email; // set email
    }, email);

    console.log('render set_login.png');
    await page.render('screens/set_login.png');

    console.log('identifierNext click');
    await page.evaluate(function() {
        var buttonNext = document.getElementById('identifierNext');
        buttonNext.click();
    });
};

set_password = async (pass) => {
    console.log('set pass');

    await page.evaluate(function(pass) {
        var inputPass = document.querySelector('input[type=password]');
        inputPass.click();
        inputPass.value = pass; // set password        
    }, pass);

    console.log('render set_pass.png');
    await page.render('screens/set_pass.png');

    console.log('passwordNext click');
    await page.evaluate(function() {
        var buttonNext = document.getElementById('passwordNext');
        buttonNext.click();
    });
};

check_login_input = () => {
    console.log('check username input');
    return page.evaluate(function() {
        var input = document.querySelector('div[role="progressbar"]');
        return (input != undefined && input != null);       
    });
};

check_pass_input = () => {
    console.log('check pass input');
    return page.evaluate(function() {
        var input = document.querySelector('div[role="progressbar"]');
        return (input != undefined && input != null);
    });
};

check_login = () => {
    console.log('check login');
    return page.evaluate(function() {
        var element = document.querySelector('div.B0hNN') != undefined;      
        return (element != undefined && element != null);
    });
};

login = async (email, password) => {
    console.log('google login');
    const status = await page.open(urls.login);

    await page.property('', {});

    if (status == "success") {
        await waitFor(check_login_input, 5000);
        await set_email(email);
        await timeout(5000);
        await waitFor(check_pass_input, 5000);
        await set_password(password);
        await timeout(5000);
        await waitFor(check_login, 5000);
        //waitFor(check_login_input, 5000);
        console.log('render result.png');
        page.render('screens/result.png');
    }
    return status;
};

exports.set_page = set_page;
exports.login = login;