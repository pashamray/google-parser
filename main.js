const fs = require('fs');
const phantom = require('phantom');
const timeout = require('./utils').timeout;
const google = require('./google');
const dashboard = require('./dashboard');
const statistics = require('./statistics');

const accounts = require('./config/accounts.json');



(async () => {
    console.log('Start parsing.');

    for (account of accounts) {
        console.log('Account: ' + account.login);
        let filename = './plugins/stats/'+ account.login +'.json';

        const instance = await phantom.create();
        let page = await instance.createPage();
        await page.property('viewportSize', { width: 1024, height: 768 });
        await page.property('customHeaders', {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36'})
        await google.set_page(page);

        console.log('login to: ' + account.login);
        const status = await google.login(account.login, account.pass);

        if(status == "success") {
            await dashboard.set_page(page);
            page = await dashboard.open();
            await timeout(5000);
            console.log('render dashboard.png');
            await page.render('screens/'+ account.login +'_dashboard.png');
            
            statistics.set_page(page);

            try {
                let plugins = null;
                await (async () => {
                    plugins = await dashboard.get_plugins();
                    for (const plugin of plugins) {
                        plugin.stats.base = await statistics.get_base(plugin.hash);
                        plugin.stats.week = await statistics.get_weekly_users(plugin.hash);
                        plugin.stats.uninstall = await statistics.get_uninstall(plugin.hash);
                    }
                })();
                fs.writeFile(filename, JSON.stringify(plugins));
            } catch(error) {
                console.log('ERROR:')
                console.error(error);
            }
        }
        await instance.exit();

    }
    console.log('Finish');
})();
