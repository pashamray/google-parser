const timeout = ms => new Promise(res => setTimeout(res, ms))

exports.timeout = timeout;