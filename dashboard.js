const urls = require('./config/urls.json');

let page = null;

async function set_page(value) {
    page = value;
}

open = async () => {
    // page.open(urls.dashboard);
    //return await page.open('http://localhost:8000');
    await page.open(urls.dashboard);
    return page;
};

get_plugins = async () => {
    return await page.evaluate(function() {
        var table = document.querySelector('div#cx-dev-dash');
        var rows = table.querySelectorAll('div[class="Ne-K m-ib m-qb-ac-u"]');        

        var plugins = [];

        for(i = 0; i < rows.length; i++) {
            var element = rows[i];
            var plugin = {};
            var root = element.querySelector('table[class="m-qb-ac-u-Wa"]');
            var td = root.querySelector('td[class="m-qb-ac-Wb"]');
            var img = td.querySelector('div[class="m-w"] > img').src;
            var name = td.querySelector('div[class="m-w"] > h2').innerText;
            var version = td.querySelector('div[class="m-w"] > div[class="m-Di"]').innerText.replace(/Version /g, '');
            var stars = td.querySelectorAll('div[class="Ne-K m-N"] > div[class="rsw-stars"] > div[class="rsw-starred"]').length;
            var comments = td.querySelector('div[class="Ne-K m-N"] > span[class="m-Di"]').innerText.replace(/[\(\)]/g, '');
            var created_at = root.querySelector('td[class="m-qb-ac-jl"]').innerText;
            var published_at = root.querySelector('td[class="m-qb-ac-yk"]').innerText;
            var users_weekly = root.querySelector('td[class="m-qb-ac-jm"]').innerText.replace(/\,/g, '');
            var status = root.querySelector('td[class*="-status"] > span').innerText.toLowerCase().replace(/ /g, '_');

            var hash = root.querySelector('td[class="m-qb-ac-zb"] div > div[class="mb-D m-Pa"] > table > tbody > tr > td').innerText;

            plugin['icon'] = img;
            plugin['name'] = name;
            plugin['version'] = version;
            plugin['stars'] = stars; 
            plugin['comments'] = comments;
            plugin['created_at'] = created_at;
            plugin['published_at'] = published_at; 
            plugin['users_weekly'] = users_weekly; 
            plugin['status'] = status;
            plugin['hash'] = hash;
            plugin['stats'] = { "base": null, "week": null, "uninstall": null };

            plugins.push(plugin);
        }
        return plugins;
    });
};

exports.set_page = set_page;
exports.open = open;
exports.get_plugins = get_plugins;